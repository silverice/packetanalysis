﻿using System;
using System.Diagnostics;
using PacketEncoder;
using PacketParser;

namespace PacketViewer
{
    public class PacketParserMock : IPacketParser
    {
        Random rnd = new Random();

        public int Count
        {
            get { return 1000; }
        }

        public string Parse(int packetIdx)
        {
            Debug.Assert(packetIdx < Count);
            return "you have selected " + packetIdx.ToString();
        }

        public PacketInfo this[int packetIdx]
        {
            get {
                Debug.Assert(packetIdx < Count);
                return new PacketInfo("SMSG_HELLO", rnd.Next(0,2) == 0);
            }
        }

        public void Load(string path)
        {
            Encoder.readFile(path);
        }
    }
}
