﻿
namespace PacketParser
{
    public interface IPacketParser
    {
        int Count { get; }

        string Parse(int packetIdx);

        PacketInfo this[int packetIdx] { get; }

        void Load(string fileName);
    }

    public struct PacketInfo
    {
        public string opcode;
        public bool direction;

        public PacketInfo(string OpCode, bool Direction)
        {
            opcode = OpCode;
            direction = Direction;
        }
    }
}
