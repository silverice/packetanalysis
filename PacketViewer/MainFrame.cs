﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PacketParser;

namespace PacketViewer
{
    public partial class MainFrame : Form
    {
        Dictionary<int, ListViewItem> cache = new Dictionary<int, ListViewItem>();
        IPacketParser packets = new PacketParserMock();

        public MainFrame()
        {
            InitializeComponent();
        }

        ListViewItem createListView(int index)
        {
            var info = packets[index];
            return new ListViewItem(new string[]{
                info.direction ? info.opcode : null,
                info.direction ? null : info.opcode,
            });
        }

        private void listIndexChanged(object sender, EventArgs e)
        {
            if (packetView.SelectedIndices.Count > 0)
                parsedView.Text = packets.Parse(packetView.SelectedIndices[0]);
        }

        private void listCacheItems(object sender, CacheVirtualItemsEventArgs e)
        {
            if (cache.ContainsKey(e.StartIndex) && cache.ContainsKey(e.EndIndex))
                return;

            // Fill the cache with the appropriate ListViewItems.
            for (int i = e.StartIndex; i <= e.EndIndex; ++i)
            {
                if (!cache.ContainsKey(i))
                    cache.Add(i, createListView(i));
            }
        }

        private void listGetVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            // check to see if the requested item is currently in the cache
            if (cache.ContainsKey(e.ItemIndex))
            {
                // A cache hit, so get the ListViewItem from the cache instead of making a new one.
                e.Item = cache[e.ItemIndex];
            }
            else
            {
                // A cache miss, so create a new ListViewItem and pass it back.
                var item = createListView(e.ItemIndex);
                e.Item = item;
                cache[e.ItemIndex] = item;
            }
        }

        private void listSearchVirtualItem(object sender, SearchForVirtualItemEventArgs e)
        {
            //e.Text
        }

        private void exitApp(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void openFile(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            cache.Clear();
            packetView.VirtualListSize = 0;

            packets.Load(openFileDialog1.FileName);
            packetView.VirtualListSize = packets.Count;
        }
    }
}
