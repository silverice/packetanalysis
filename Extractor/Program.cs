﻿using System;
using System.Collections.Generic;
using System.Linq;
using PacketEncoder;
using PacketAnalysis;
using PacketAnalysis.Data;
using PacketAnalysis.Data.cata_434;
using System.IO;
using System.Collections;
using System.Text;
using System.Reflection.Emit;
using System.Reflection;
using System.Diagnostics;
using PacketAnalysis.Utility;

namespace Extractor
{
    public class MonsterTextComparer : IComparer<MonsterText>, IEqualityComparer<MonsterText>
    {
        #region IComparer<MonsterText> Members

        public int Compare(MonsterText left, MonsterText right)
        {
            int val = left.source.Entry.CompareTo(right.source.Entry);
            if (val != 0)
                return val;
            return left.text.CompareTo(right.text);
        }

        #endregion

        #region IEqualityComparer<MonsterText> Members

        public bool Equals(MonsterText x, MonsterText y)
        {
            return Compare(x, y) == 0;
        }

        public int GetHashCode(MonsterText obj)
        {
            return obj.text.GetHashCode();
        }

        #endregion
    }

    class Program
    {
        public static void extractTexts(ParserMgr mgr, string[] paths)
        {
            var opcode = mgr.getOpcode<MonsterText>();
            var texts = new List<MonsterText>();
            var entries = new HashSet<uint>();

            string str;
            foreach (var path in paths)
            {
                var sniff = PacketEncoder.Encoder.readFile(path);
                foreach (var text in mgr.parseAll<MonsterText>(sniff))
                {
                    if (text.source.Entry == 0)
                        continue;
                    str = text.ToString();
                    texts.Add(text);
                    entries.Add(text.source.Entry);
                }
            }

            texts.Sort(new MonsterTextComparer());
            var textsUnique = texts.Distinct(new MonsterTextComparer());
            texts = null;

            var list = new List<string>();

            var delete = "delete from locales_creature_text where entry in (";
            foreach (var entry in entries)
                delete += string.Join(", ", entries.Select((val) => { return val.ToString(); }).ToArray());
            delete += ");";

            list.Add(delete);

            list.Add("insert into locales_creature_text (entry, textGroup, id, text_loc8) values");
            uint currentry = 0;
            uint id = 0;
            foreach (var text in textsUnique)
            {
                if (text.source.Entry == 0)
                    continue;

                if (currentry == text.source.Entry)
                    ++id;
                else {
                    id = 0;
                    currentry = text.source.Entry;
                }
                list.Add(String.Format("({0}, 0, {1}, \"{2}\"),",
                    text.source.Entry,
                    id,
                    text.text));
            }

            File.WriteAllLines(paths[0] + "_locales_creature_text.sql", list.ToArray());
        }

        public static void extractSpellCasts(ParserMgr mgr, string[] paths)
        {
            var casts = new Dictionary<uint, HashSet<uint>>();

            foreach (var file in PacketEncoder.Encoder.readFiles(paths))
            {
                foreach (var spell in mgr.parseAll<SpellGo>(file))
                {
                    var entry = spell.caster.Entry;
                    if (entry != 0)
                    {
                        if (casts.ContainsKey(entry))
                            casts[entry].Add(spell.spellId);
                        else
                            casts.Add(entry, new HashSet<uint> { spell.spellId });
                    }
                }
            }

            var bl = new StringBuilder();

            foreach (var cast in casts)
            {
                bl.AppendFormat("-{0}{1}", cast.Key, Environment.NewLine);

                bl.AppendLine(string.Join(
                    Environment.NewLine,
                    cast.Value.Select((val) => { return val.ToString(); }).ToArray()
                    )
                );
            }
            File.WriteAllText(paths[0] + "casts.txt", bl.ToString());
        }

        delegate void Command(ParserMgr mgr, string[] paths);

        static void Main(string[] args)
        {
            var d = new MonsterMove();
            Console.WriteLine(d.ToString());
            return;

            if (args.Length == 0)
            {
                args = new string[] { @"C:\Development\cata\logs\15595_2012-8-7_0-2_worgen_mage_part1.dump" };
            }

            Dictionary<string, Command> commands = new Dictionary<string, Command>();
            foreach (var meth in typeof(Program).GetMethods())
            {
                if (meth.GetParameters().Length != 2)
                    continue;
                commands.Add(meth.Name, (Command)Delegate.CreateDelegate(typeof(Command), meth));
                Console.WriteLine(meth.Name);
            }
            Console.WriteLine();
            Console.WriteLine("what can i do for you?");

            var mgr = new ParserMgr(ClientBuild.b434_45595);
            mgr.loadParsers(typeof(ParserMgr).Assembly);

            while (true)
            {
                var line = Console.ReadLine();

                if (args.Length == 0)
                {
                    Console.WriteLine("nothing to process with");
                    continue;
                }

                Command command;
                if (commands.TryGetValue(line, out command))
                {
                    Console.WriteLine("executing: {0}", line);
                    command(mgr, args);
                    Console.WriteLine("done");
                }
                else
                    break;
            }
        }

        public class SomeDataBase
        {
            public Int64 z = 10;
            public string y = "SomeDataBase field-string";
        }

        public class SomeData : SomeDataBase
        {
            static readonly ToStringMethod method = ToStringGen.Generate(typeof(SomeData));

            public Int64 a = 10;
            public string b = "tomato";

            public override string ToString()
            {
                var builder = new StringBuilder();
                method(builder, this);
                return builder.ToString();
            }

            //public string Str { get; set; }
        }
    }
}
