﻿using System;
using System.Collections.Generic;
using System.Reflection;
using PacketEncoder;
using System.Diagnostics;

namespace PacketAnalysis
{
    // Class that intended to parse raw packets of some specific client build
    public class ParserMgr
    {
        ClientBuild m_version;

        Dictionary<int /*opcode*/, ParseFunc> m_parsers = new Dictionary<int, ParseFunc>();
        Dictionary<Type/*parsed data type*/, int /*opcode*/> m_data2Opcode = new Dictionary<Type,int>();

        delegate Parsed ParseFunc(RawPacket packet);

        public const int noOpcode = -1;

        public ParserMgr(ClientBuild version)
        {
            m_version = version;
        }

        public void loadParsers(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                foreach (var method in type.GetMethods())
                {
                    var attrs = (ParserAttribute[])method.GetCustomAttributes(typeof(ParserAttribute), false);
                    if (attrs.Length == 0)
                        continue;

                    var param = method.GetParameters();
                    if (param.Length != 1 /*|| param[0].GetType() != typeof(RawPacket)*/)
                        continue;

                    Debug.Assert(attrs.Length == 1);
                    var atribute = attrs[0];
                    if (atribute.Version != m_version)
                        continue;

                    var dataType = method.ReturnType;
                    while (!dataType.IsDefined(typeof(AssociatedAttribute), false))
                        dataType = dataType.BaseType;

                    m_data2Opcode.Add(dataType, atribute.Opcode);
                    //foreach (var atr in attrs)
                    {

                        Debug.Assert(atribute.Opcode != 0);
                        m_parsers.Add(
                            atribute.Opcode,
                            (ParseFunc)Delegate.CreateDelegate(typeof(ParseFunc), method)
                        );
                    }
                }
            }
        }

        public T parse<T>(RawPacket packet) where T : Parsed
        {
            return (T)parse(packet);
        }

        public List<T> parseAll<T>(Sniff sniff) where T : Parsed
        {
            Debug.Assert(sniff.clientBuild == 0 || sniff.clientBuild == (uint)m_version);
            return parseAll<T>(sniff.packets);
        }

        public List<T> parseAll<T>(ICollection<RawPacket> packets) where T : Parsed
        {
            var list = new List<T>();
            var opcode = getOpcode<T>();
            foreach (var packet in packets)
            {
                if (packet.opcode == opcode)
                {
                    var data = parse<T>(packet);
                    if (data != null)
                        list.Add(data);
                }
            }
            return list;
        }

        public Parsed parse(RawPacket packet)
        {
            // using client version and packet opcode get parser
            ParseFunc parser;
            if (m_parsers.TryGetValue(packet.opcode, out parser))
                return parser(packet);
            else
                return null;
        }

        // Returns raw opcode value that is associated with parsed type or ParserMgr.noOpcode if no such opcode found
        public int getOpcode<T>() where T : Parsed
        {
            int opcode = noOpcode;
            m_data2Opcode.TryGetValue(typeof(T), out opcode);
            return opcode;
        }
    }

    public abstract class Parsed
    {
    }

    public enum ClientBuild : uint
    {
        b335_12340 = 12340,
        b434_45595 = 45595,
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public sealed class ParserAttribute : Attribute
    {
        public ClientBuild Version;
        public int Opcode;

        public ParserAttribute(ClientBuild version, int opcode)
        {
            Version = version;
            Opcode = opcode;
        }
    }

    // Specifies that target class (class that inherits Parsed class) can be associated with opcode
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class AssociatedAttribute : Attribute
    {
        public AssociatedAttribute() {}
    }
}
