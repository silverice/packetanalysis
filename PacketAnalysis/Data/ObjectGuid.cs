﻿using System;
using PacketAnalysis.Data.Enums;

namespace PacketAnalysis.Data
{
    public struct ObjectGuid
    {
        //public static readonly ObjectGuid Zero = new ObjectGuid(0);

        public readonly ulong raw;

        //public readonly ulong Low;
        //public readonly uint Entry;
        //public readonly HighGuidType HighType;
        //public readonly bool HasEntry;

        public ObjectGuid(ulong guid)
        {
            raw = guid;
        }

        public bool HasEntry
        {
            get
            {
                switch (HighType)
                {
                    case HighGuidType.Unit:
                    case HighGuidType.GameObject:
                    case HighGuidType.Vehicle:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public HighGuidType HighType
        {
            get {
                if (raw == 0)
                    return HighGuidType.None;
                var highGUID = (HighGuidType)((raw & 0xF0F0000000000000) >> 52);
                return highGUID == 0 ? HighGuidType.Player : highGUID;
            }
        }

        public ObjectType ObjectType
        {
            get {
                switch (HighType)
                {
                    case HighGuidType.Player:
                        return ObjectType.Player;
                    case HighGuidType.DynObject:
                        return ObjectType.DynamicObject;
                    case HighGuidType.Item:
                        return ObjectType.Item;
                    case HighGuidType.GameObject:
                    case HighGuidType.Transport:
                    case HighGuidType.MOTransport:
                        return ObjectType.GameObject;
                    case HighGuidType.Vehicle:
                    case HighGuidType.Unit:
                    case HighGuidType.Pet:
                        return ObjectType.Unit;
                    default:
                        return ObjectType.Object;
                }
            }
        }

        public ulong Low
        {
            get {
                switch (HighType)
                {
                    case HighGuidType.Player:
                    case HighGuidType.DynObject:
                    case HighGuidType.Group:
                    case HighGuidType.Item:
                        return raw & 0x000FFFFFFFFFFFFF;
                    case HighGuidType.GameObject:
                    case HighGuidType.Transport:
                    case HighGuidType.MOTransport:
                    case HighGuidType.Vehicle:
                    case HighGuidType.Unit:
                    case HighGuidType.Pet:
                        return raw & 0x00000000FFFFFFFF;
                }
                // TODO: check if entryless guids don't use now more bytes
                return raw & 0x00000000FFFFFFFF;
            }
        }

        public uint Entry
        {
            get {
                return HasEntry ? (uint)((raw & 0x000FFFFF00000000) >> 32) : 0;
            }
        }

        public static bool operator ==(ObjectGuid first, ObjectGuid other)
        {
            return first.raw == other.raw;
        }

        public static bool operator !=(ObjectGuid first, ObjectGuid other)
        {
            return !(first == other);
        }

        public override bool Equals(object obj)
        {
            return obj != null && obj is Guid && Equals((Guid)obj);
        }

        public bool Equals(ObjectGuid other)
        {
            return other.raw == raw;
        }

        public override int GetHashCode()
        {
            return raw.GetHashCode();
        }

        public override string ToString()
        {
            return "0x" + raw.ToString("X");
            /*if (m_guid == 0)
                return "0x0";

            // If our guid has an entry and it is an unit or a GO, print its
            // name next to the entry (from a database, if enabled)
            if (HasEntry())
            {
                var type = Utilities.ObjectTypeToStore(GetObjectType());

                return "m_guid: 0x" + m_guid.ToString("X8") + " Type: " + GetHighType()
                    + " Entry: " + StoreGetters.GetName(type, (int)GetEntry()) + " Low: " + GetLow();
            }

            var name = StoreGetters.GetName(this);

            switch (GetHighType())
            {
                case HighGuidType.BattleGround1:
                {
                    var bgType = m_guid & 0x00000000000000FF;
                    return "m_guid: 0x" + m_guid.ToString("X8") + " Type: " + GetHighType()
                        + " BgType: " + StoreGetters.GetName(StoreNameType.Battleground, (int)bgType);
                }
                case HighGuidType.BattleGround2:
                {
                    var bgType    = (m_guid & 0x00FF0000) >> 16;
                    var unkId     = (m_guid & 0x0000FF00) >> 8;
                    var arenaType = (m_guid & 0x000000FF) >> 0;
                    return "m_guid: 0x" + m_guid.ToString("X8") + " Type: " + GetHighType()
                        + " BgType: " + StoreGetters.GetName(StoreNameType.Battleground, (int)bgType)
                        + " Unk: " + unkId + (arenaType > 0 ? (" ArenaType: " + arenaType) : String.Empty);
                }
            }

            return "m_guid: 0x" + m_guid.ToString("X8") + " Type: " + GetHighType()
                + " Low: " + GetLow() + (String.IsNullOrEmpty(name) ? String.Empty : (" Name: " + name));*/
        }
    }
}
