﻿using PacketAnalysis.Data.Enums;
using PacketAnalysis.Utility;

namespace PacketAnalysis.Data
{
    [AssociatedAttribute]
    public class MonsterText : Parsed
    {
        public ObjectGuid source;
        public Language language;
        public ChatMessageType messageType;
        public string text;
        public ChatTag tag;

        static readonly ToStringMethod toString = ToStringGen.Generate(typeof(MonsterText));

        public override string ToString()
        {
            return ToStringGen.Invoke(this, toString);
        }
    }
}
