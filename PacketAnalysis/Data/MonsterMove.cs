﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using PacketAnalysis.Utility;

namespace PacketAnalysis.Data
{
    public enum FinalFacingType : byte
    {
        None,
        Spot,
        Target,
        Angle,
    }

    [StructLayout(LayoutKind.Explicit, Pack = 1)]
    public class FinalFacing
    {
        [FieldOffset(0)]
        public FinalFacingType type;
        [FieldOffset(1)]
        public Vector3 spot;
        [FieldOffset(1)]
        public ulong target;
        [FieldOffset(1)]
        public float angle;

        static readonly ToStringMethod toStr = ToStringGen.Generate(typeof(FinalFacing));

        public override string ToString()
        {
            return ToStringGen.Invoke(this, toStr);
        }
    }

    [AssociatedAttribute]
    public class MonsterMove : Parsed
    {
        public ObjectGuid source;
        public FinalFacing facing = new FinalFacing();
        public List<Vector3> path;

        static readonly ToStringMethod toStr = ToStringGen.Generate(typeof(MonsterMove));

        public string ToString()
        {
            return ToStringGen.Invoke(this, toStr);
        }
    }
}
