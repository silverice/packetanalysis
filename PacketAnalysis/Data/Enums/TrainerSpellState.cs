namespace PacketAnalysis.Data.Enums
{
    public enum TrainerSpellState
    {
        Green = 0,
        Red = 1,
        Grey = 2
    }
}
