﻿namespace PacketAnalysis.Data.Enums
{
    public enum  CalendarSendEventType
    {
        Get = 0,
        Add = 1,
        Copy = 2,
    }
}
