namespace PacketAnalysis.Data.Enums
{
    public enum InstanceStatus
    {
        NotSaved = 0,
        Saved = 1,
        Completed = 2
    }
}
