namespace PacketAnalysis.Data.Enums
{
    public enum Direction
    {
        ClientToServer = 0,
        ServerToClient = 1
    }
}
