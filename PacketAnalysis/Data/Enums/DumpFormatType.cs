﻿namespace PacketAnalysis.Data.Enums
{
    public enum DumpFormatType
    {
        None,           // No dump at all
        Text,
        Pkt,
        PktSplit
    }
}
