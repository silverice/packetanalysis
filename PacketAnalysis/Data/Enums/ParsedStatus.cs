namespace PacketAnalysis.Data.Enums
{
    public enum ParsedStatus
    {
        None,
        Success,
        WithErrors,
        NotParsed
    }
}
