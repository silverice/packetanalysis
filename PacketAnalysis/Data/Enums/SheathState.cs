namespace PacketAnalysis.Data.Enums
{
    public enum SheathState
    {
        Unarmed = 0,
        Melee   = 1,
        Ranged  = 2
    }
}
