﻿using System;
using PacketAnalysis.Data.Enums;
using PacketEncoder;

namespace PacketAnalysis.Data.cata_434
{
    public static class ChatHandler
    {
        [Parser(ClientBuild.b434_45595, (int)Opcode.SMSG_MESSAGECHAT)]
        public static MonsterText HandleServerChatMessage(RawPacket raw)
        {
            var text = new MonsterText();
            var packet = new PacketReader(raw);

            text.messageType = packet.ReadEnum<ChatMessageType>(TypeCode.Byte);
            text.language = packet.ReadEnum<Language>(TypeCode.Int32);
            var guid = packet.ReadGuid();
            text.source = guid;

            /*uint entry = 0;
            if (guid.GetObjectType() == ObjectType.Unit)
                entry = guid.GetEntry();*/

            packet.ReadInt32(/*"Constant time"*/);

            switch (text.messageType)
            {
                case ChatMessageType.Channel:
                    {
                        packet.ReadCString(/*"Channel Name"*/);
                        goto case ChatMessageType.Say;
                    }
                case ChatMessageType.Say:
                case ChatMessageType.Yell:
                case ChatMessageType.Party:
                case ChatMessageType.PartyLeader:
                case ChatMessageType.Raid:
                case ChatMessageType.RaidLeader:
                case ChatMessageType.RaidWarning:
                case ChatMessageType.Guild:
                case ChatMessageType.Officer:
                case ChatMessageType.Emote:
                case ChatMessageType.TextEmote:
                case ChatMessageType.Whisper:
                case ChatMessageType.WhisperInform:
                case ChatMessageType.System:
                case ChatMessageType.Battleground:
                case ChatMessageType.BattlegroundLeader:
                case ChatMessageType.Achievement:
                case ChatMessageType.GuildAchievement:
                case ChatMessageType.Restricted:
                case ChatMessageType.Dnd:
                case ChatMessageType.Afk:
                    {
                        packet.ReadGuid(/*"Sender GUID"*/);
                        break;
                    }
                case ChatMessageType.BattlegroundNeutral:
                case ChatMessageType.BattlegroundAlliance:
                case ChatMessageType.BattlegroundHorde:
                    {
                        var target = packet.ReadGuid(/*"Sender GUID"*/);
                        switch (target.HighType)
                        {
                            case HighGuidType.Unit:
                            case HighGuidType.Vehicle:
                            case HighGuidType.GameObject:
                            case HighGuidType.Transport:
                            case HighGuidType.Pet:
                                packet.ReadInt32(/*"Sender Name Length"*/);
                                packet.ReadCString(/*"Sender Name"*/);
                                break;
                        }
                        break;
                    }
                case ChatMessageType.MonsterSay:
                case ChatMessageType.MonsterYell:
                case ChatMessageType.MonsterParty:
                case ChatMessageType.MonsterEmote:
                case ChatMessageType.MonsterWhisper:
                case ChatMessageType.RaidBossEmote:
                case ChatMessageType.RaidBossWhisper:
                case ChatMessageType.BattleNet:
                    {
                        packet.ReadInt32(/*"Name Length"*/);
                       /* text.Comment = */packet.ReadCString(/*"Name"*/);

                        var target = packet.ReadGuid(/*"Receiver GUID"*/);
                        switch (target.HighType)
                        {
                            case HighGuidType.Unit:
                            case HighGuidType.Vehicle:
                            case HighGuidType.GameObject:
                            case HighGuidType.Transport:
                                packet.ReadInt32(/*"Receiver Name Length"*/);
                                /*text.Comment += " to " + */packet.ReadCString(/*"Receiver Name"*/);
                                break;
                        }
                        break;
                    }
                case ChatMessageType.Ignored:
                    {
                        packet.ReadGuid(/*"Sender GUID"*/);
                        packet.ReadInt32(/*"String Length"*/);
                        packet.ReadCString(/*"Unk String"*/);
                        packet.ReadByte(/*"Unk Byte"*/);
                        return text;
                    }
            }

            if (/*ClientVersion.AddedInVersion(ClientVersionBuild.V4_1_0_13914) && */text.language == Language.Addon)
                packet.ReadCString(/*"Addon Message Prefix"*/);

            packet.ReadInt32(/*"Text Length"*/);
            text.text = packet.ReadCString(/*"Text"*/);
            text.tag = packet.ReadEnum<ChatTag>(TypeCode.Byte);

            //if (ClientVersion.AddedInVersion(ClientVersionBuild.V4_2_0_14333))
            {
                if (text.messageType == ChatMessageType.RaidBossEmote || text.messageType == ChatMessageType.RaidBossWhisper)
                {
                    packet.ReadSingle(/*"Unk single"*/);
                    packet.ReadByte(/*"Unk byte"*/);
                }
            }

            if (text.messageType == ChatMessageType.Achievement || text.messageType == ChatMessageType.GuildAchievement)
                packet.ReadInt32(/*"Achievement ID"*/);
            return text;
        }
    }
}
