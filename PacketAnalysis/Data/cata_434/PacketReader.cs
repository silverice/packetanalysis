﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PacketEncoder;
using Microsoft.Xna.Framework;

namespace PacketAnalysis.Data.cata_434
{
    public class PacketReader : BinaryReader
    {
        public PacketReader(RawPacket packet)
            : base(new MemoryStream(packet.data), Encoding.ASCII)
        {
        }

        public string ReadCString()
        {
            return ReadCString(Encoding.UTF8);
        }

        public string ReadCString(Encoding encoding)
        {
            var bytes = new List<byte>();

            byte b;
            while (CanRead() && (b = ReadByte()) != 0)  // CDataStore::GetCString calls CanRead too
                bytes.Add(b);

            return encoding.GetString(bytes.ToArray());
        }

        public bool CanRead()
        {
            return BaseStream.Position != BaseStream.Length;
        }

        public T ReadEnum<T>(TypeCode code) where T : struct
        {
            var value = ReadValue(code);
            return (T)Enum.ToObject(typeof(T), value);
        }

        public ObjectGuid ReadGuid()
        {
            return new ObjectGuid(ReadUInt64());
        }

        public Vector3 ReadVector3()
        {
            return new Vector3
            {
                X = ReadSingle(), Y = ReadSingle(), Z = ReadSingle()
            };
        }

        public ObjectGuid ReadPackedGuid()
        {
            byte mask = ReadByte();

            if (mask == 0)
                return new ObjectGuid(0);

            ulong res = 0;

            int i = 0;
            while (i < 8)
            {
                if ((mask & 1 << i) != 0)
                    res += (ulong)ReadByte() << (i * 8);

                i++;
            }

            return new ObjectGuid(res);
        }

        private long ReadValue(TypeCode code)
        {
            long rawValue = 0;
            switch (code)
            {
                case TypeCode.SByte:
                    rawValue = ReadSByte();
                    break;
                case TypeCode.Byte:
                    rawValue = ReadByte();
                    break;
                case TypeCode.Int16:
                    rawValue = ReadInt16();
                    break;
                case TypeCode.UInt16:
                    rawValue = ReadUInt16();
                    break;
                case TypeCode.Int32:
                    rawValue = ReadInt32();
                    break;
                case TypeCode.UInt32:
                    rawValue = ReadUInt32();
                    break;
                case TypeCode.Int64:
                    rawValue = ReadInt64();
                    break;
                case TypeCode.UInt64:
                    rawValue = (long)ReadUInt64();
                    break;
                default:
                    throw new ArgumentException();
            }
            return rawValue;
        }
    }
}
