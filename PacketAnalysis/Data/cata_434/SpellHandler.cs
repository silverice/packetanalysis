﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PacketEncoder;
using PacketAnalysis.Data.Enums;
using PacketAnalysis.Utility;

namespace PacketAnalysis.Data.cata_434
{
    public static class SpellHandler
    {
        static void read(PacketReader rd, ref SpellTargetInfo info)
        {
            var targetFlags = info.flags = rd.ReadEnum<TargetFlag>(TypeCode.Int32);

            if (targetFlags.HasAnyFlag(TargetFlag.Unit | TargetFlag.CorpseEnemy | TargetFlag.GameObject |
                TargetFlag.CorpseAlly | TargetFlag.UnitMinipet))
                info.target = rd.ReadPackedGuid();

            if (targetFlags.HasAnyFlag(TargetFlag.Item | TargetFlag.TradeItem))
                info.targetItem = rd.ReadPackedGuid();

            if (targetFlags.HasAnyFlag(TargetFlag.SourceLocation))
            {
                //if (ClientVersion.AddedInVersion(ClientVersionBuild.V3_2_0_10192))
                    info.sourceTransport = rd.ReadPackedGuid();

                info.sourceTransportOffset = rd.ReadVector3();
            }

            if (targetFlags.HasAnyFlag(TargetFlag.DestinationLocation))
            {
                //if (ClientVersion.AddedInVersion(ClientVersionBuild.V3_0_8_9464))
                    info.destTransport = rd.ReadPackedGuid();

                info.desteTransportOffset = rd.ReadVector3();
            }

            if (targetFlags.HasAnyFlag(TargetFlag.NameString))
                info.targetString = rd.ReadCString();
        }

        //[Parser(ClientBuild.b434_45595, Opcode.SMSG_SPELL_START)]
        [Parser(ClientBuild.b434_45595, (int)Opcode.SMSG_SPELL_GO)]
        public static SpellGo HandleSpellStart(RawPacket rawpacket)
        {
           // bool isSpellGo = packet.opcode == Opcode.SMSG_SPELL_GO;
            var rd = new PacketReader(rawpacket);
            SpellGo spell = new SpellGo();

            spell.caster = rd.ReadPackedGuid();
            spell.casterUnit = rd.ReadPackedGuid();
            spell.castCount = rd.ReadSByte();

            spell.spellId = rd.ReadUInt32();

            var flags = spell.castFlags = rd.ReadEnum<CastFlag>(TypeCode.Int32);

            //packet.ReadUInt32("Time");
            spell.time = rd.ReadUInt32();
            spell.time2 = rd.ReadUInt32();

            //if (isSpellGo)
            {
                spell.hitGuids = Util.ReadNTimes<ObjectGuid>((int)rd.ReadByte(), (val) =>
                {
                    return rd.ReadGuid();
                });

                spell.miss = Util.ReadNTimes<SpellGo.MissInfo>((int)rd.ReadByte(), (val) => {
                    val.guid = rd.ReadGuid();
                    val.missType = rd.ReadEnum<SpellMissType>(TypeCode.Byte);
                    val.missReflect = (val.missType == SpellMissType.Reflect ? rd.ReadEnum<SpellMissType>(TypeCode.Byte) : (SpellMissType)0);
                    return val;
                });
            }

            read(rd, ref spell.target);

            //if (ClientVersion.AddedInVersion(ClientVersionBuild.V3_0_2_9056))
            {
                if (flags.HasAnyFlag(CastFlag.PredictedPower))
                    spell.runeCooldown = rd.ReadInt32();

                if (flags.HasAnyFlag(CastFlag.RuneInfo))
                {
                    var spellRuneState = spell.spellRuneState = rd.ReadByte();
                    var playerRuneState = spell.playerRuneState = rd.ReadByte();

                    for (var i = 0; i < 6; i++)
                    {
                        //if (ClientVersion.RemovedInVersion(ClientVersionBuild.V4_2_2_14545))
                        {
                            var mask = 1 << i;
                            if ((mask & spellRuneState) == 0)
                                continue;

                            if ((mask & playerRuneState) != 0)
                                continue;
                        }

                        rd.ReadByte(/*"Rune Cooldown Passed", i*/);
                    }
                }

                //if (isSpellGo)
                {
                    if (flags.HasAnyFlag(CastFlag.AdjustMissile))
                    {
                        spell.missileElevation = rd.ReadSingle();
                        spell.missileDelayTime = rd.ReadInt32();
                    }
                }
            }

            if (flags.HasAnyFlag(CastFlag.Projectile))
            {
                spell.ammoDisplayId = rd.ReadUInt32();
                spell.ammoInventory = rd.ReadEnum<InventoryType>(TypeCode.Int32);
            }

            if (flags.HasAnyFlag(CastFlag.VisualChain))
            {
                rd.ReadInt32(/*"Unk Int32 2"*/);
                rd.ReadInt32(/*"Unk Int32 3"*/);
            }

            var targetFlags = spell.target.flags;

            if (targetFlags.HasAnyFlag(TargetFlag.DestinationLocation))
                rd.ReadSByte(/*"Unk Byte 2"*/); // Some count

            if (targetFlags.HasAnyFlag(TargetFlag.ExtraTargets))
            {
                spell.targetsExtra = Util.ReadNTimes<SpellGo.TartetExtra>(rd.ReadInt32(), (val) => {
                    val.targetPos = rd.ReadVector3();
                    val.guid = rd.ReadGuid();
                    return val;
                });
            }
            return spell;
        }
    }

}
