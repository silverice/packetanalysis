﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PacketAnalysis.Data.Enums;
using Microsoft.Xna.Framework;

namespace PacketAnalysis.Data
{
    public struct SpellTargetInfo
    {
        // spell cast targets
        public TargetFlag flags;
        public ObjectGuid target;
        public ObjectGuid targetItem;

        public ObjectGuid sourceTransport;
        public Vector3 sourceTransportOffset;

        public ObjectGuid destTransport;
        public Vector3 desteTransportOffset;

        public string targetString;
    }

    public class SpellCastShared : Parsed
    {
        public ObjectGuid caster;
        public ObjectGuid casterUnit;
        public sbyte castCount;
        public uint spellId;
        public CastFlag castFlags;
        public uint time;
        public uint time2; // 430

        public SpellTargetInfo target;

        public int runeCooldown;
        public byte spellRuneState;
        public byte playerRuneState;

        public uint ammoDisplayId;
        public InventoryType ammoInventory;
    }

    [AssociatedAttribute]
    public class SpellGo : SpellCastShared
    {
        public List<ObjectGuid> hitGuids;

        public struct MissInfo
        {
            public ObjectGuid guid;
            public SpellMissType missType;
            public SpellMissType missReflect;
        }
        public List<MissInfo> miss;

        public float missileElevation;
        public int missileDelayTime;

        public struct TartetExtra
        {
            public Vector3 targetPos;
            public ObjectGuid guid;
        }
        public List<TartetExtra> targetsExtra;
    }
}
