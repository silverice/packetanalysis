﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection.Emit;
using System.Reflection;

namespace PacketAnalysis.Utility
{
    public delegate void ToStringMethod(StringBuilder builder, object obj);

    public static class MethodGen
    {
        public static string Invoke(object obj, ToStringMethod method)
        {
            var str = new StringBuilder();
            method(str, obj);
            return str.ToString();
        }

        public static ToStringMethod GenerateToString(Type type)
        {
            var method = new DynamicMethod(
                "",
                typeof(void),
                new Type[] { typeof(StringBuilder), typeof(object) },
                type.Module,
                true
            );

            var il = method.GetILGenerator();
            il.Emit(OpCodes.Ldarg_0);
            foreach (var field in type.GetFields())
                EmitFieldWrite(il, field);
            il.Emit(OpCodes.Pop);
            il.Emit(OpCodes.Ret);

            return (ToStringMethod)method.CreateDelegate(typeof(ToStringMethod));
        }

        static void EmitFieldWrite(ILGenerator il, FieldInfo field)
        {
            var appendStringMethod = typeof(StringBuilder).GetMethod("Append", new Type[] { typeof(string) });
            // 0                1
            // StringBuilder    object
            EmitStringAppend(il, field.Name); // builder
            EmitStringAppend(il, " : "); // builder

            if (!field.FieldType.IsValueType) // reference type
            {
                var objToStr = typeof(object).GetMethod("ToString", new Type[0]);
                il.Emit(OpCodes.Ldarg_1);  // builder, obj     
                il.Emit(OpCodes.Ldfld, field); // builder, field
                il.Emit(OpCodes.Callvirt, objToStr); // builder, string
            }
            else
            {
                var toStr = field.FieldType.GetMethod("ToString", new Type[0]);
                if (toStr.DeclaringType == field.FieldType)
                {
                    il.Emit(OpCodes.Ldarg_1);  // builder, obj     
                    il.Emit(OpCodes.Ldflda, field); // builder, field
                    il.Emit(OpCodes.Call, toStr); // builder, string
                }
                else
                {
                    var objToStr = typeof(object).GetMethod("ToString", new Type[0]);
                    il.Emit(OpCodes.Ldarg_1);  // builder, obj     
                    il.Emit(OpCodes.Ldflda, field); // builder, field
                    il.Emit(OpCodes.Constrained, field.FieldType);
                    il.Emit(OpCodes.Callvirt, objToStr); // builder, string
                }
            }
            il.Emit(OpCodes.Call, appendStringMethod); // builder
            EmitStringAppend(il, Environment.NewLine); // builder
        }

        // builder
        // builder
        static void EmitStringAppend(ILGenerator il, string str)
        {
            var appendStringMethod = typeof(StringBuilder).GetMethod("Append", new Type[] { typeof(string) });
            il.Emit(OpCodes.Ldstr, str); // builder, str
            il.Emit(OpCodes.Call, appendStringMethod); // builder
        }
    }
}
