﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PacketAnalysis.Utility
{
    public static class Util
    {
        public static List<T> ReadNTimes<T>(int times, Func<T, T> act) where T : struct
        {
            var type = new T();
            var list = new List<T>(times);
            for (var i = 0; i < times;  ++i)
                list.Add(act(type));
            return list;
        }

        /// <summary>
        /// Returns true if flag exists in value (&)
        /// </summary>
        /// <param name="value">An enum, int, ...</param>
        /// <param name="flag">An enum, int, ...</param>
        /// <returns>A boolean</returns>
        public static bool HasAnyFlag(this IConvertible value, IConvertible flag)
        {
            var uFlag = flag.ToUInt64(null);
            var uThis = value.ToUInt64(null);

            return (uThis & uFlag) != 0;
        }
    }
}
