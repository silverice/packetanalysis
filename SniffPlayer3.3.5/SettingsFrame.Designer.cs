﻿namespace SniffPlayer335
{
    partial class SettingsFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonDropFilter = new System.Windows.Forms.Button();
            this.buttonNewFilter = new System.Windows.Forms.Button();
            this.comboBoxFilterList = new System.Windows.Forms.ComboBox();
            this.dataGridIgnored = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.opcodeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIgnored)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opcodeFilterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.buttonDropFilter);
            this.splitContainer1.Panel1.Controls.Add(this.buttonNewFilter);
            this.splitContainer1.Panel1.Controls.Add(this.comboBoxFilterList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridIgnored);
            this.splitContainer1.Size = new System.Drawing.Size(387, 303);
            this.splitContainer1.SplitterDistance = 45;
            this.splitContainer1.TabIndex = 0;
            // 
            // buttonDropFilter
            // 
            this.buttonDropFilter.Location = new System.Drawing.Point(286, 10);
            this.buttonDropFilter.Name = "buttonDropFilter";
            this.buttonDropFilter.Size = new System.Drawing.Size(89, 23);
            this.buttonDropFilter.TabIndex = 2;
            this.buttonDropFilter.Text = "Delete Filter";
            this.buttonDropFilter.UseVisualStyleBackColor = true;
            this.buttonDropFilter.Click += new System.EventHandler(this.deleteFilter);
            // 
            // buttonNewFilter
            // 
            this.buttonNewFilter.Location = new System.Drawing.Point(197, 10);
            this.buttonNewFilter.Name = "buttonNewFilter";
            this.buttonNewFilter.Size = new System.Drawing.Size(83, 23);
            this.buttonNewFilter.TabIndex = 1;
            this.buttonNewFilter.Text = "New Filter";
            this.buttonNewFilter.UseVisualStyleBackColor = true;
            this.buttonNewFilter.Click += new System.EventHandler(this.newFilter);
            // 
            // comboBoxFilterList
            // 
            this.comboBoxFilterList.FormattingEnabled = true;
            this.comboBoxFilterList.Location = new System.Drawing.Point(3, 12);
            this.comboBoxFilterList.Name = "comboBoxFilterList";
            this.comboBoxFilterList.Size = new System.Drawing.Size(179, 21);
            this.comboBoxFilterList.TabIndex = 0;
            this.comboBoxFilterList.SelectedIndexChanged += new System.EventHandler(this.selectedFilterChanged);
            // 
            // dataGridIgnored
            // 
            this.dataGridIgnored.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridIgnored.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridIgnored.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridIgnored.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column1});
            this.dataGridIgnored.Location = new System.Drawing.Point(3, 3);
            this.dataGridIgnored.Name = "dataGridIgnored";
            this.dataGridIgnored.Size = new System.Drawing.Size(381, 248);
            this.dataGridIgnored.TabIndex = 0;
            this.dataGridIgnored.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.cellEditOpc);
            this.dataGridIgnored.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ignored opcode";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "movement opcode";
            this.Column1.Name = "Column1";
            // 
            // opcodeFilterBindingSource
            // 
            this.opcodeFilterBindingSource.DataSource = typeof(SniffPlayer335.OpcodeFilter);
            // 
            // SettingsFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 303);
            this.Controls.Add(this.splitContainer1);
            this.Name = "SettingsFrame";
            this.Text = "SettingsFrame";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIgnored)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opcodeFilterBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button buttonDropFilter;
        private System.Windows.Forms.Button buttonNewFilter;
        private System.Windows.Forms.ComboBox comboBoxFilterList;
        private System.Windows.Forms.BindingSource opcodeFilterBindingSource;
        private System.Windows.Forms.DataGridView dataGridIgnored;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;


    }
}