﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using SniffPlayer335.Properties;

namespace SniffPlayer335
{
    public partial class SettingsFrame : Form
    {
        public SettingsFrame()
        {
            InitializeComponent();

            comboBoxFilterList.Items.AddRange(OpcodeFilter.Filters);
        }

        int selectedFilter
        {
            get { return comboBoxFilterList.SelectedIndex; }
        }

        string selectedFilterName
        {
            get { return (string)comboBoxFilterList.Items[selectedFilter]; }
        }

        private void selectedFilterChanged(object sender, EventArgs e)
        {
            updateGridView();
        }

        void updateGridView()
        {
            dataGridIgnored.Rows.Clear();

            var lists = OpcodeFilter.getFilter(selectedFilterName);
            if (lists == null)
                return;

            int count = lists.Max((list) => {
                return list.Count; 
            });

            dataGridIgnored.Rows.Add(count);

            for (int column = 0; column < lists.Length; ++column)
            {
                var list = lists[column];
                var itr = list.GetEnumerator();
                for (int row = 0; row < list.Count; ++row, itr.MoveNext())
                    dataGridIgnored.Rows[row].Cells[column].Value = itr.Current.ToString("X");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cellEditOpc(object sender, DataGridViewCellEventArgs e)
        {
            var lists = OpcodeFilter.getFilter(selectedFilterName);
            var list = lists[e.ColumnIndex];
            list.Clear();

            for (int row = 0; row < dataGridIgnored.Rows.Count; ++row)
            {
                object value = dataGridIgnored.Rows[row].Cells[e.ColumnIndex].Value;
                if (value == null)
                    continue;
                try
                {
                    int opcode = Int32.Parse(value.ToString(),
                        NumberStyles.HexNumber);
                    list.Add(opcode);
                }
                catch (ArgumentException) { }
            }
            Settings.Default.Save();
        }

        private void newFilter(object sender, EventArgs e)
        {

        }

        private void deleteFilter(object sender, EventArgs e)
        {

        }
    }
}
