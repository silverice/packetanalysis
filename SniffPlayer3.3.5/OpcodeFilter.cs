﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SniffPlayer335.Properties;
using System;

namespace SniffPlayer335
{
    public static class OpcodeFilter
    {
        static readonly Dictionary<string, HashSet<int>[]> filters = new Dictionary<string, HashSet<int>[]>();

        static OpcodeFilter()
        {
            //Settings.Default.ignoredOpcodes = string.Empty;
            if (Settings.Default.ignoredOpcodes.Length != 0)
            {
                foreach (var filter in Settings.Default.ignoredOpcodes.Split('{', '}'))
                {
                    if (string.IsNullOrEmpty(filter))
                        continue;
                    var strings = filter.Split(';');
                    var name = strings[0];
                    var opc = new HashSet<int>[]
                    {
                        new HashSet<int>(strings[1].Split(',').Select(x => int.Parse(x))),
                        new HashSet<int>(strings[2].Split(',').Select(x => int.Parse(x))),
                    };

                    filters.Add(name, opc);
                }
            }
            else
            {
                filters.Add("default",
                    new HashSet<int>[] {
                        new HashSet<int>(new int[] { 1, 2, 3, 4 }),
                        new HashSet<int>(new int[] { 1, 2, 3, 4 })
                    }
                );
            }

            Settings.Default.SettingsSaving += (obj, args) =>
            {
                // save settings
                string data = String.Empty;
                foreach (var pair in filters)
                {
                    data += '{';
                    data += pair.Key + ';';
                    data += string.Join(",", pair.Value[0].Select(n => n.ToString()).ToArray()) + ';';
                    data += string.Join(",", pair.Value[1].Select(n => n.ToString()).ToArray());
                    data += '}';
                }
                Settings.Default.ignoredOpcodes = data;
            };
        }

        public static string[] Filters
        {
            get { return OpcodeFilter.filters.Keys.ToArray(); }
        }

        public static HashSet<int>[] getFilter(string name)
        {
            HashSet<int>[] flt;
            OpcodeFilter.filters.TryGetValue(name, out flt);
            return flt;
        }

        public static HashSet<int>[] newFilter()
        {
            return new HashSet<int>[] {
                        new HashSet<int>(new int[] { 0 }),
                        new HashSet<int>(new int[] { 0 })
                    };
        }

        public static void setFilter(string name, HashSet<int>[] opc)
        {
            Debug.Assert(!name.Contains(';'));
            Debug.Assert(!name.Contains(','));
            OpcodeFilter.filters[name] = opc;
        }
    }
}
