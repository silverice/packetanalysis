﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using PacketEncoder;
using SniffPlayer335.Properties;
using System.Xml;
using SniffPlayer335;
using System.Configuration;

namespace SniffPlayer
{
    public interface ISniffPlayer
    {
        bool running { get; set; }
        float rate { get; set; }
        int timePassed { get; }
        int timeTotal { get; }

        void tick(int timeDiff);
    }

    public partial class MainFrame : Form
    {
        Timer timer = new Timer();
        TcpClient client = new TcpClient();

        List<RawPacket> packets;
        List<int> scenes = new List<int>();

        uint time = 0;
        int packetIdx = 0;

        public MainFrame()
        {
            InitializeComponent();

            timer.Interval = 50;
            timer.Tick += new EventHandler(onTick);

            rate = 1;
            movementsEnabled = true;
            try
            {
                client.Connect(Settings.Default.host, Settings.Default.port);
            }
            catch { }
        }

        private void openFile(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            var encoder = Encoder.readFile(openFileDialog1.FileName);
            var pckts = encoder.packets;

            var sc = new List<int>();
            for (int idx = 0; idx < pckts.Count; ++idx)
                if (pckts[idx].opcode == (int)Opcode.SMSG_NEW_WORLD)
                    sc.Add(idx);

            if (sc.Count == 0)
                return;

            packets = pckts;
            scenes = sc;
            reset();
        }

        bool validate
        {
            get { return packets != null; }
        }

        void reset()
        {
            running = false;
            packetIdx = 0;
            if (packets != null)
                time = (uint)packets[packetIdx].tickTime;
        }

        void tryConnect()
        {
            if (client.Connected)
                return;
            //client.Client.Disconnect(true);
            try
            {
               // client.Connect(Settings.Default.host, Settings.Default.port);
            }
            catch (System.Exception)
            {
                //client.Client.Disconnect(true);
            }
        }

        bool running
        {
            get { return timer.Enabled; }
            set
            {
                if (running == value)
                    return;

                if (value){
                    if (validate)
                    {
                        btRunPause.Text = "||";
                        timer.Enabled = true;
                    }
                }
                else  {
                    btRunPause.Text = ">";
                    timer.Enabled = false;
                }
            }
        }

        #region Event handlers
        private void onStop(object sender, EventArgs e)
        {
            reset();
        }

        private void onRunPause(object sender, EventArgs e)
        {
            running = !running;
        }

        private void onPrev(object sender, EventArgs e)
        {
            if (!validate)
                return;
            var scene = scenes.BinarySearch(packetIdx);

            if (scene < 0)
                scene = ~scene;

            if (scene > 0) {
                packetIdx = scenes[scene - 1];
                time = (uint)packets[packetIdx].tickTime;
            }
        }

        private void onNext(object sender, EventArgs e)
        {
            if (!validate)
                return;
            var next = scenes.FirstOrDefault((idx) => { return idx > packetIdx; });
            if (next > 0)
            {
                packetIdx = next;
                time = (uint)packets[packetIdx].tickTime;
            }
        }

        float rate
        {
            get {
                return 2 * (trackBar1.Value - trackBar1.Minimum) / (float)(trackBar1.Maximum - trackBar1.Minimum);
            }
            set {
                trackBar1.Value = (int)(0.5 * value * (trackBar1.Maximum - trackBar1.Minimum) + trackBar1.Minimum);
            }
        }

        bool movementsEnabled
        {
            get { return checkBox1.Checked; }
            set { checkBox1.Checked = value; }
        }

        #endregion

        void onTick(object sender, EventArgs e)
        {
            uint diff = (uint)(timer.Interval * rate);
            time += diff;
            while (true) {
                if (packetIdx >= packets.Count) {
                    running = false;
                    break;
                }
                if (packets[packetIdx].tickTime > time)
                    break;
                sendPacket(packets[packetIdx]);
                ++packetIdx;
            }
        }

        void sendPacket(RawPacket packet)
        {
            //tryConnect();

            if (opcodesIgnore.Contains(packet.opcode))
                return;

            if (packet.direction == PacketDirection.ServerToClient ||
                (movementsEnabled && opcodesMovement.Contains(packet.opcode)))
            {
                using (var args = new SocketAsyncEventArgs())
                {
                    int datasize = 2 + packet.data.Length;
                    var data = new byte[4 + 4 + datasize];
                    var writer = new BinaryWriter(new MemoryStream(data));
                    writer.Write((int)1);
                    writer.Write(IPAddress.HostToNetworkOrder(datasize));
                    writer.Write((ushort)packet.opcode);
                    writer.Write(packet.data);
                    args.SetBuffer(data, 0, data.Length);
                    client.Client.SendAsync(args);
                }
            }
        }

        readonly HashSet<int> opcodesMovement = new HashSet<int>(/*new Opcode[]{
            Opcode.MSG_MOVE_START_FORWARD,
            Opcode.MSG_MOVE_START_BACKWARD,
            Opcode.MSG_MOVE_STOP,
            Opcode.MSG_MOVE_START_STRAFE_LEFT,
            Opcode.MSG_MOVE_START_STRAFE_RIGHT,
            Opcode.MSG_MOVE_STOP_STRAFE,
            Opcode.MSG_MOVE_JUMP,
            Opcode.MSG_MOVE_START_TURN_LEFT,
            Opcode.MSG_MOVE_START_TURN_RIGHT,
            Opcode.MSG_MOVE_STOP_TURN,
            Opcode.MSG_MOVE_START_PITCH_UP,
            Opcode.MSG_MOVE_START_PITCH_DOWN,
            Opcode.MSG_MOVE_STOP_PITCH,
            Opcode.MSG_MOVE_SET_RUN_MODE,
            Opcode.MSG_MOVE_SET_WALK_MODE,
            Opcode.MSG_MOVE_FALL_LAND,
            Opcode.MSG_MOVE_START_SWIM,
            Opcode.MSG_MOVE_STOP_SWIM,
            Opcode.MSG_MOVE_SET_FACING,
            Opcode.MSG_MOVE_SET_PITCH,
            Opcode.MSG_MOVE_HEARTBEAT,
            Opcode.MSG_MOVE_START_ASCEND,
            Opcode.MSG_MOVE_STOP_ASCEND,
            Opcode.MSG_MOVE_START_DESCEND,
        }*/);

        readonly HashSet<int> opcodesIgnore = new HashSet<int>(/*new Opcode[]{
            Opcode.SMSG_WARDEN_DATA,
            Opcode.SMSG_AUTH_CHALLENGE,
            Opcode.SMSG_CHAR_ENUM,
            Opcode.SMSG_RESUME_COMMS,
        }*/);

        private void showSettings(object sender, EventArgs e)
        {
            var frm = new SettingsFrame();
            frm.Show(this);
        }
    }
}
