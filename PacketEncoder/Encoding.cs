﻿using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;

namespace PacketEncoder
{
    public static class Encoder
    {
        internal static readonly Dictionary<string, IEncoder> encoders = new Dictionary<string, IEncoder>();
        public static readonly IEncoder Pkt = new PktEncoder();
        public static readonly IEncoder TextDump = new TextDumpEncoder();

        static Encoder()
        {
            registerEncoder(".pkt", Pkt);
            registerEncoder(".dump", TextDump);
        }

        public static void registerEncoder(string fileExtenstion, IEncoder encoder)
        {
            if (fileExtenstion[0] != '.')
                throw new ArgumentException();
            encoders.Add(fileExtenstion, encoder);
        }

        public static string[] registeredExtensions()
        {
            return encoders.Keys.ToArray();
        }

        public static IEncoder getEncoder(string fileExtension)
        {
            IEncoder encoder;
            encoders.TryGetValue(fileExtension.ToLowerInvariant(), out encoder);
            return encoder;
        }

        public static Sniff readFile(string path)
        {
            return getEncoder(Path.GetExtension(path)).readFile(path);
        }

        public static IEnumerable<Sniff> readFiles(IEnumerable<string> paths)
        {
            return paths.Select((path)=>readFile(path));
        }
    }

    public abstract class IEncoder
    {
        public abstract Sniff read(Stream stream);
        public abstract void write(Stream stream, Sniff packets);

        public void writeFile(string path, Sniff packets)
        {
            using (var stream = new FileStream(path, FileMode.CreateNew))
                write(stream, packets);
        }

        public Sniff readFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open))
                return read(stream);
        }
    }

    public sealed class Sniff
    {
        public readonly List<RawPacket> packets = new List<RawPacket>();
        public uint clientBuild;
        //public uint clientLocale;  // locale
        public byte[] sessionKey; // can be null
    }

    public sealed class RawPacket
    {
        public int opcode;
        public int tickTime;
        public PacketDirection direction;
        public byte[] data;
    }

    public enum PacketDirection : byte
    {
        ClientToServer,
        ServerToClient,
    }
}
