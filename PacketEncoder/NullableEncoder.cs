﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PacketEncoder
{
    internal class NullableEncoder : IEncoder
    {
        public override Sniff read(System.IO.Stream stream)
        {
            return null;
        }

        public override void write(System.IO.Stream stream, Sniff packets)
        {
        }
    }
}
