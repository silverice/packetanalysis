﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PacketEncoder
{
    public class PktEncoder : IEncoder
    {
        enum PktVersion
        {
            NoHeader = 0,
            V2_1 = 0x201,
            V2_2 = 0x202,
            V3_0 = 0x300,
            V3_1 = 0x301,
        }

        public override Sniff read(System.IO.Stream stream)
        {
            using (var reader = new BinaryReader(stream))
            {
                var headerStart = reader.ReadBytes(3);             // PKT
                if (Encoding.ASCII.GetString(headerStart) != "PKT")
                    throw new Exception();

                var pktVersion = (PktVersion)reader.ReadUInt16();      // sniff version
                var packets = new Sniff();

                switch (pktVersion)
                {
                    case PktVersion.V2_1:
                    {
                        packets.clientBuild = reader.ReadUInt16();
                        reader.ReadBytes(40); // session key
                        break;
                    }
                    case PktVersion.V2_2:
                    {
                        reader.ReadByte();                         // sniffer id
                        packets.clientBuild = reader.ReadUInt16();
                        reader.ReadBytes(4);                       // client locale
                        reader.ReadBytes(20);                      // packet key
                        reader.ReadBytes(64);                      // realm name
                        break;
                    }
                    case PktVersion.V3_0:
                    {
                        var snifferId = reader.ReadByte();         // sniffer id
                        packets.clientBuild = reader.ReadUInt32();
                        reader.ReadBytes(4);                       // client locale
                        reader.ReadBytes(40);                      // session key
                        var additionalLength = reader.ReadInt32();
                        var preAdditionalPos = reader.BaseStream.Position;
                        reader.ReadBytes(additionalLength);
                        var postAdditionalPos = reader.BaseStream.Position;
                        if (snifferId == 10)                        // xyla
                        {
                            reader.BaseStream.Position = preAdditionalPos;
                            reader.ReadUInt32();   // start time
                            reader.ReadUInt32(); // start tick count
                            reader.BaseStream.Position = postAdditionalPos;
                        }
                        break;
                    }
                    case PktVersion.V3_1:
                    {
                        reader.ReadByte();                         // sniffer id
                        packets.clientBuild = reader.ReadUInt32();
                        reader.ReadBytes(4);                       // client locale
                        reader.ReadBytes(40);                      // session key
                        reader.ReadUInt32(); // start time
                        reader.ReadUInt32();     // start tick count
                        var additionalLength = reader.ReadInt32();
                        reader.ReadBytes(additionalLength);
                        break;
                    }
                    default:
                        throw new Exception();
                }

                while (reader.BaseStream.Position < reader.BaseStream.Length)
                    packets.packets.Add(readPacket(reader, pktVersion));
                return packets;
            }
        }

        static RawPacket readPacket(BinaryReader reader, PktVersion version)
        {
            PacketDirection direction;
            int ticks = 0;
            int length;
            int opcode;
            byte[] data;

            switch (version)
            {
                case PktVersion.V2_1:
                case PktVersion.V2_2:
                {
                    direction = (reader.ReadByte() == 0xff) ? PacketDirection.ServerToClient : PacketDirection.ClientToServer;
                    reader.ReadInt32(); //unix time
                    ticks = reader.ReadInt32(); // tick count
                    length = reader.ReadInt32();

                    if (direction == PacketDirection.ServerToClient)
                    {
                        opcode = reader.ReadInt16();
                        data = reader.ReadBytes(length - 2);
                    }
                    else
                    {
                        opcode = reader.ReadInt32();
                        data = reader.ReadBytes(length - 4);
                    }
                    break;
                }
                case PktVersion.V3_0:
                case PktVersion.V3_1:
                {
                    direction = (reader.ReadUInt32() == 0x47534d53) ? PacketDirection.ServerToClient : PacketDirection.ClientToServer;

                    if (version == PktVersion.V3_0)
                    {
                        reader.ReadInt32(); //unix time
                        ticks = (int)reader.ReadUInt32();
                    }
                    else
                    {
                        reader.ReadUInt32(); // session id
                        ticks = (int)reader.ReadUInt32();
                    }

                    int additionalSize = reader.ReadInt32();
                    length = reader.ReadInt32();
                    reader.ReadBytes(additionalSize);
                    opcode = reader.ReadInt32();
                    data = reader.ReadBytes(length - 4);
                    break;
                }
                default:
                {
                    opcode = reader.ReadUInt16();
                    length = reader.ReadInt32();
                    direction = (PacketDirection)reader.ReadByte();
                    reader.ReadInt64(); //unix
                    data = reader.ReadBytes(length);
                    break;
                }
            }

            return new RawPacket()
            {
                opcode = opcode,
                direction = direction,
                tickTime = ticks,
                data = data,
            };
        }

        public override void write(System.IO.Stream stream, Sniff packets)
        {
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(Encoding.ASCII.GetBytes("PKT"));
                writer.Write((ushort)PktVersion.V2_1);      // sniff version
                writer.Write((ushort)packets.clientBuild);
                writer.Write(new byte[40]); // session key

                foreach(var packet in packets.packets)
                    writePacket(writer, packet);
            }
        }

        static void writePacket(BinaryWriter writer, RawPacket packet)
        {
            writer.Write((byte)(packet.direction == PacketDirection.ServerToClient ? 0xff : 0));
            writer.Write((int)0); // unix time
            writer.Write((int)packet.tickTime);

            if (packet.direction == PacketDirection.ServerToClient)
            {
                writer.Write((int)(packet.data.Length + 2));
                writer.Write((short)packet.opcode);
                writer.Write(packet.data);
            }
            else
            {
                writer.Write((int)(packet.data.Length + 4));
                writer.Write((int)packet.opcode);
                writer.Write(packet.data);
            }
        }
    }
}
