﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Xml;

namespace PacketEncoder
{
    internal class TextDumpEncoder : IEncoder
    {
        public override Sniff read(Stream stream)
        {
            var bytes = new byte[stream.Length - stream.Position];
            Debug.Equals(stream.Read(bytes, 0, bytes.Length), bytes.Length);

            using (var reader = new StringReader(Encoding.ASCII.GetString(bytes)))
            {
                Sniff packets = new Sniff();
                while (reader.Peek() != -1)
                {
                    var lines = reader.ReadLine().Split(' ', ';');
                    var time = Int32.Parse(lines[1]);
                    var direction = (lines[3] == "SMSG" ? PacketDirection.ServerToClient : PacketDirection.ClientToServer);
                    var opcode = Int32.Parse(lines[5]);
                    var data = HexToBytes(lines[7]);
                    packets.packets.Add(new RawPacket()
                    {
                        opcode = opcode,
                        tickTime = time,
                        direction = direction,
                        data = data,
                    });
                }
                return packets;
            }
        }

        //Time: 26656703;OpcodeType: SMSG;OpcodeValue: 18183;Packet: CF5BEE8B01800702A4FB000039015500983A0000983A0000;
        public override void write(Stream stream, Sniff packets)
        {
            foreach (var p in packets.packets)
            {
                var line = String.Format("Time: {0};OpcodeType: {1};OpcodeValue: {2};Packet: {3};{4}",
                    p.tickTime,
                    p.direction == PacketDirection.ServerToClient ? "SMSG" : "CMSG",
                    p.opcode,
                    ToHex(p.data),
                    Environment.NewLine);
                var bytes = Encoding.ASCII.GetBytes(line);
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        static string ToHex(byte[] bytes)
        {
            var c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }

            return new string(c);
        }

        static byte[] HexToBytes(string str)
        {
            if ((str.Length % 2) != 0)
            {
                throw new ArgumentException("Invalid length: " + str.Length);
            }

            int sx = str.StartsWith("0x") ? 2 : 0;
            byte[] buffer = new byte[str.Length / 2];
            char c;
            for (int bx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte
                c = str[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

                // Convert second half of byte
                c = str[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }

            return buffer;
        }
    }
}
